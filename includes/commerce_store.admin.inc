<?php

/**
 * @file
 * Administrative callbacks and form builder functions for Commerce Stock.
 */

/**
 * Commerce Stock admin form.
 */
function commerce_store_admin_form($form, &$form_state) {
  global $commerce_store;

  $form['store_open'] = array(
    '#type' => 'checkbox',
    '#default_value' => $commerce_store->getStoreOpen(),
    '#title' => t('Store Open'),
  );


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}


/**
 * Add or remove the Stock field from product types.
 */
function commerce_store_admin_form_submit($form, &$form_state) {
  $store_open = $form_state['values']['store_open'];
  global $commerce_store;
  $commerce_store->setStoreOpen($store_open);
  $commerce_store->PersistData();
}