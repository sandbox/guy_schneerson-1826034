<?php

/**
 * @file
 * The Store class.
 *
 * Used to store the settings in a single variable thats is lazy loaded
 * when accessed using a getter or setter.
 * To add properties to the class add them to the $def_settings array
 * and create getter and setter functions (see StoreOpen)
 */


class CommerceStore {

  private $settings;
  private $dataLoaded;

  function __construct() {
    $this->dataLoaded = FALSE;
  }

  private function makesureIsLoaded() {
    // make sure data is loaded
     if (!$this->dataLoaded) {
       $this->loadData();
     }
  }

  public function loadData() {
    // default settings
    $def_settings = array(
        'store_open' => TRUE,
    );
    // load from db
    $store_settings = variable_get('commerce_store_settings', serialize($def_settings));
    $store_settings = unserialize($store_settings);
    // merge making sure stored values overide defaults
    $this->settings =array_merge($def_settings, $store_settings);
    // set to loaded
    $this->dataLoaded = TRUE;
  }

  public function PersistData() {
    // @todo persist data
    variable_set('commerce_store_settings', serialize($this->settings));
  }


  public function getStoreOpen() {
    // make sure data is loaded
     $this->makesureIsLoaded();
     // return the value
     return $this->settings['store_open'];
  }

  public function setStoreOpen($open) {
    // make sure data is loaded
     $this->makesureIsLoaded();
     // set value
     $this->settings['store_open'] = $open;
  }
}