<?php

/**
 * Expose book properties as a context condition.
 */
class commerce_store_condition_is_open extends context_condition {
  function condition_values() {
    // @todo: only one of should be selectable
    $values = array();
    $values['open'] = 'Store is open';
    $values['closed'] = 'Store is closed';
    return $values;
  }

  function execute($value) {
    foreach ($this->get_contexts($value) as $context) {
      $this->condition_met($context, $value);
    }
  }
}
