<?php

/**
 * @file
 * Rules integration for Commerce Store.
 */


/**
 * Implements hook_rules_condition_info().
 */
function commerce_store_rules_condition_info() {
  $conditions = array();

  $conditions['commerce_store_is_open'] = array(
    'label' => t('Is the commerce store open for online sales'),
    'group' => t('Commerce Store'),
    'callbacks' => array(
      'execute' => 'commerce_store_rule_is_open',
    ),
  );

  return $conditions;
}


/**
 *
 * Rules condition: checks to see if store is open.
 */
function commerce_store_rule_is_open() {
  global $commerce_store;
  if (isset($commerce_store)) {
    return $commerce_store->getStoreOpen();
  }
  else {
    // should never run but just in case cant find the data then
    // fallback to default behaviour.
    return TRUE;
  }

}